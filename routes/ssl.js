module.exports = function(app){

function randenv() {
	return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
}

var multer = require('multer');
var upload = multer({ dest: __dirname + '/../upload'});

var crypto = require("crypto");

var pem = require('pem');

var md5 = require('js-md5');

var request = require("request");

var shelljs = require('shelljs');

var moment = require('moment');

var async = require('async');

var fs = require('fs-extra');

var dns = require('dns');

// functions

function geturlhost(url) {

var tu = url;

var protocheck = url.indexOf('://');

if (protocheck != -1) {
        tu = url.substring(protocheck + 3, url.length);
}

var ua = tu.split('');

var endofhostname = tu.length;

for (s = 0; s < ua.length; s++) {
        if (!/\w|\d|-|\./.test(ua[s]) || s == ua.length) {
                endofhostname = s;
                break;
        }
}

return (tu.substring(0,endofhostname));

}


// ssl routes

app.route ("/api/v1/certinfo")
.post(function(req,res,next){

var now = moment();

var resultbundle = {};

var parsedcert = pem.readCertificateInfo(req.body.ci_certinput, function(err, output){

if (output != undefined) {

resultbundle.commonname = output.commonName;
resultbundle.issuer = output.issuer.commonName;
resultbundle.serial = output.serial.replace(/:/g,'');
resultbundle.validfrom = moment(output.validity.start).format('MMMM Do YYYY');
resultbundle.validto = moment(output.validity.end).format('MMMM Do YYYY');
resultbundle.organization = output.organization;
resultbundle.locality = output.locality;
resultbundle.state = output.state;
resultbundle.country = output.country;

if (output.san !== undefined) {
resultbundle.sans = output.san.dns;
} else {
resultbundle.sans = '';
}

var certstatus = '';

if (now < moment(output.validity.start)) {
certstatus = 'Not active yet';
}

if (now > moment(output.validity.end)) {
certstatus = '<style color="red"><span class="glyphicon glyphicon-remove"></span></style> <b>Expired</b>';
}

if (now > moment(output.validity.start) && now < moment(output.validity.end)) {
certstatus = '<span class="glyphicon glyphicon-ok green"></span> <b>Valid</b> - expires in ' + moment.duration(now.diff(output.validity.end)).humanize() + '';
}

resultbundle.certstatus = certstatus;
resultbundle.error = '';
} else {
 resultbundle.error = 'Error: could not read certificate data';
}

res.send (resultbundle);

});

});

app.route ("/api/v1/csrinfo")
.post(function(req,res,next){

var now = moment();

var resultbundle = {};

var parsedcsr = pem.readCertificateInfo(req.body.ci_certinput, function(error, output){

resultbundle.commonname = output.commonName;
resultbundle.organization = output.organization;
resultbundle.locality = output.locality;
resultbundle.state = output.state;
resultbundle.country = output.country;

res.send (resultbundle);

});

});



app.route ("/api/v1/p7binfo")
.post(function(req,res,next){

var now = moment();

var resultbundle = {};

var randenvstr1 = randenv();

shelljs.env[randenvstr1] = req.body.ci_certinput;

shelljs.exec('echo "$' + randenvstr1 + '" | openssl pkcs7 -print_certs | openssl x509', {silent:true}, function(code, stdout, stderr) {

var parsedcsr = pem.readCertificateInfo(stdout, function(error, output){

if (output != undefined) {

resultbundle.commonname = output.commonName;
resultbundle.issuer = output.issuer.commonName;
resultbundle.serial = output.serial.replace(/:/g,'');
resultbundle.validfrom = moment(output.validity.start).format('MMMM Do YYYY');
resultbundle.validto = moment(output.validity.end).format('MMMM Do YYYY');
resultbundle.organization = output.organization;
resultbundle.locality = output.locality;
resultbundle.state = output.state;
resultbundle.country = output.country;

if (output.san !== undefined) {
resultbundle.sans = output.san.dns;
} else {
resultbundle.sans = '';
}

var certstatus = '';

if (now < moment(output.validity.start)) {
certstatus = 'Not active yet';
}

if (now > moment(output.validity.end)) {
certstatus = '<style color="red"><span class="glyphicon glyphicon-remove"></span></style> <b>Expired</b>';
}

if (now > moment(output.validity.start) && now < moment(output.validity.end)) {
certstatus = '<span class="glyphicon glyphicon-ok green"></span> <b>Valid</b> - expires in ' + moment.duration(now.diff(output.validity.end)).humanize() + '';
}

resultbundle.certstatus = certstatus;
resultbundle.error = '';
} else {
 resultbundle.error = 'Error: could not read certificate data';
}

randenvstr1 = '';
shelljs.env[randenvstr1] = '';

res.send (resultbundle);


});
});

});




app.route ("/api/v1/pfxinfo")
.post(function(req,res,next){

var randenvstr1 = randenv();
var randenvstr2 = randenv();
var randenvstr3 = randenv();
var randenvstr4 = randenv();
var randenvstr5 = randenv();
var randenvstr6 = randenv();

shelljs.env[randenvstr1] = req.body.ci_pfxinput.split(',')[1];
//shelljs.env[randenvstr2] = req.body.ci_pfxpwd;
shelljs.ShellString(req.body.ci_pfxpwd).to('/tmp/' + randenvstr2); // write out the password to a temp file to be used in openssl
shelljs.exec(
randenvstr3 + '=$(echo -n $' + randenvstr1 + ' | base64 -d | openssl pkcs12 -nomacver -nocerts -nodes -password file:/tmp/' + randenvstr2 + ' 2>/tmp/' + randenvstr6 + ' | openssl rsa 2>/dev/null); ' +
randenvstr4 + '=$(echo -n $' + randenvstr1 + ' | base64 -d | openssl pkcs12 -nomacver -nokeys -clcerts -nodes -password file:/tmp/' + randenvstr2 + ' | openssl x509 2>/dev/null); ' +
randenvstr5 + '=$(echo -n $' + randenvstr1 + ' | base64 -d | openssl pkcs12 -nomacver -nokeys -cacerts -nodes -password file:/tmp/' + randenvstr2 + ' | openssl x509 2>/dev/null); ' +
'printf \'{"key": "%s","cert": "%s","chain": "%s", "err": "%s"}\' "$' + randenvstr3 + '" "$' + randenvstr4 + '" "$' + randenvstr5 + '" "$(cat /tmp/' + randenvstr6 + ')" ; rm /tmp/' + randenvstr6 + '; ' +
'rm /tmp/' + randenvstr2
 , {silent:true}, function(code, stdout, stderr) { 

 var resdata = { resvalue: Buffer.from(stdout).toString('base64') };
 res.send(resdata);
 });

shelljs.env[randenvstr1] = '';
shelljs.env[randenvstr2] = '';
shelljs.env[randenvstr3] = '';
shelljs.env[randenvstr4] = '';
shelljs.env[randenvstr5] = '';
shelljs.env[randenvstr6] = '';

randenvstr1 = '';
randenvstr2 = '';
randenvstr3 = '';
randenvstr4 = '';
randenvstr5 = '';
randenvstr6 = '';

});






app.route ("/api/v1/certca")
.post(function(req,res,next){

var randenvstr = randenv();
shelljs.env[randenvstr] = req.body.ccr_certinput;
shelljs.exec('echo "$' + randenvstr + '" | ' + __dirname + '/ccr -i' , {silent:true}, function(code, stdout, stderr) {
 var resdata = { result1: stderr, result2: stdout };
 res.send(resdata);
 });

shelljs.env[randenvstr] = '';
randenvstr = '';

});


app.route ("/api/v1/modhash")
.post(function(req,res,next){

resultbundle = {};

pem.getModulus (req.body.ckm_data, function(err,modhashobj){

pem.readCertificateInfo(req.body.ckm_data, function(err,stdout){

switch (req.body.ckm_type) {
 case 'x509':
 if (stdout != undefined) {
  resultbundle.commonname = stdout.commonName;
  resultbundle.modhash = md5(modhashobj.modulus);
 } else {
    resultbundle.commonname = '';
    resultbundle.modhash = 'Error: could not read certificate data';
 }
 break;

 case 'rsa':
 if (typeof modhashobj === 'undefined') {
  resultbundle.modhash = '';
  resultbundle.rescode = 1;
 } else {
  resultbundle.modhash = md5(modhashobj.modulus);
  resultbundle.rescode = 0;
 }
 break;
}

res.send (resultbundle);

});

});

});



app.route ("/api/v1/getkey")
.post(function(req,res,next){

//ignore untrusted cert for the request get calls
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var url = 'https://INF%5Csvc_jenkins:70a779dbabc4c82b41376becb6a61145@bp-dvmh-ci-04.inf.bulletproof.net/job/create-ssl-certificate-signing-request/lastSuccessfulBuild/api/json?pretty=true';

pem.readCertificateInfo(req.body.ckm_data, function(err,stdout){

if (!err) { // input certificate was parsed successfully

request(url, function(error, response, respbody) {
var searchstring = stdout.commonName.replace ('*','star');

//build search array
var resultsarray = [];
for (var i = 0; i < JSON.parse(respbody).artifacts.length; i++) {
var arrstring = JSON.parse(respbody).artifacts[i].fileName;
if ((arrstring.search (searchstring) != -1) && (arrstring.substr(arrstring.length-4,4) == '.key')) {
resultsarray.push (arrstring);
}

}

if (resultsarray.length > 0) {
// iterate through results and download each file, and check its modulus

pem.getModulus(req.body.ckm_data, function(certerr, certresults) {
// the cert's modulus is in certresults.modulus

// QUAID, START THE LOOP!
async.detect(resultsarray, function(key, callback) {

var url = 'https://INF%5Csvc_jenkins:70a779dbabc4c82b41376becb6a61145@bp-dvmh-ci-04.inf.bulletproof.net/job/create-ssl-certificate-signing-request/lastSuccessfulBuild/artifact/' + key;

request(url, function(error, response, respbody) { // download each key

pem.getModulus(respbody, function (keyerr, keyresults) {

// check if the current key matches the cert, and send the boolean result back up to the async.detect callback
matchsuccess = (!keyerr && (certresults.modulus === keyresults.modulus));

callback(null, matchsuccess);

}); // pem.getModulus

}); // request(keyheader
	
}, function(err, result) {

var resultsbundle = {};

if (typeof result == 'undefined') { // result is undefined if the end of the list is reached without a match
resultsbundle.rescode = 2;
resultsbundle.reslist = resultsarray;
res.send (resultsbundle);
} else {

var url = 'https://INF%5Csvc_jenkins:70a779dbabc4c82b41376becb6a61145@bp-dvmh-ci-04.inf.bulletproof.net/job/create-ssl-certificate-signing-request/lastSuccessfulBuild/artifact/' + result;

request(url, function(error, response, respbody) { 

var resultsbundle = {}; 
resultsbundle.rescode = 0;
resultsbundle.reskey = respbody;
res.send (resultsbundle);

});

}

});


}); // pem.getModulus(req.body.ckm_data


} else {
// resultsarray is empty
var resultsbundle = {};
resultsbundle.rescode = 1; // no matches could be found in artifact json object
res.send (resultsbundle);
}


});

} else { //!err
var resultsbundle = {};
resultsbundle.rescode = 2; // the cert could not be parsed
res.send (resultsbundle);

}


});

});


app.route ('/api/v1/checkcert')
.post(function(req,res,next){

var randenvstr = randenv();
shelljs.env[randenvstr] = req.body.cck_servername;

shelljs.exec('openssl s_client -showcerts -connect "$' + randenvstr + '":443 -servername "$' + randenvstr + '" </dev/null', {silent:true},  function(code, stdout, stderr) {
 res.send (stdout + stderr);
 });

});




app.route ('/api/v1/convertkey')
.post(function(req,res,next){

var randenvstr = randenv();
shelljs.env[randenvstr] = req.body.ckm_keyconvertinput;

shelljs.exec('echo "$' + randenvstr + '" | openssl rsa',  {silent:true},  function(code, stdout, stderr) {
 
 var resultbundle = {};

 switch (code) {
	case 0:
		resultbundle.rescode = 0;
		resultbundle.keyoutput = stdout;
		break;

	default:
		resultbundle.rescode = 1;
		resultbundle.keyoutput = '0';
	}

 res.send (resultbundle);	

 });

});





app.route ("/api/v1/p7bsplit")
.post(function(req,res,next){

var randenvstr1 = randenv();
shelljs.env[randenvstr1] = req.body.cp_certdata;

shelljs.exec('echo "$' + randenvstr1 + '" | openssl pkcs7 -print_certs', {silent:true}, function(code, stdout, stderr) {

var inputstr = stdout;
var certs = [];

var lines = inputstr.split('\n');

var cert = 0;

var writing_cert = false;

var resultbundle = {};

for(var i = 0; i < lines.length; i++) {

if (lines[i].substring(0,27) == '-----BEGIN CERTIFICATE-----' ) { // certificate has been detected, start writing to array

if (writing_cert) { // if the opening line is found but already in writing mode, something's wrong
resultbundle.rescode = 2; // malformed cert found
res.send (resultbundle);
break;
}

writing_cert = true;
cert += 1;
certs[cert] = '';
}

if (writing_cert) {

switch (true) {
 case lines[i].substring(0,8) === 'subject=':
 case lines[i].substring(0,7) === 'issuer=':
  break;

 case lines[i].length != 0:  
  certs[cert] = certs[cert] + lines[i] + '\n';
  break;
}

}

if (lines[i].substring(0,25) == '-----END CERTIFICATE-----' ) { // end of cert detected, stop writing to array
writing_cert = false;
}


} // for loop

resultbundle.cert = '';
resultbundle.chain = '';

if (certs.length > 1) {
 resultbundle.cert = certs[1];
 resultbundle.rescode = 0;

if (certs.length > 2) {

for (var i = 2; i < certs.length; i++) {
 resultbundle.chain = resultbundle.chain + certs[i];
}
resultbundle.rescode = 0;
}

} else {
resultbundle.rescode = 1; // no certs were found
}

res.send (resultbundle);

});

});




app.post ('/api/v1/cppfxupload', upload.any(), function(req,res){

var f = req.files[0];

var randenvstr1 = randenv();
var randenvstr2 = randenv();
var randenvstr3 = randenv();
var randenvstr4 = randenv();
var randenvstr5 = randenv();
var randenvstr6 = randenv();

//shelljs.env[randenvstr2] = req.body.pwd;
shelljs.ShellString(req.body.pwd).to('/tmp/' + randenvstr2); // write out the password to a temp file to be used in openssl
shelljs.exec(
randenvstr3 + '=$(openssl pkcs12 -in ' + f.path + ' -nomacver -nocerts -nodes -password file:/tmp/' + randenvstr2 + ' 2>/tmp/' + randenvstr6 + ' | openssl rsa 2>/dev/null); ' +
randenvstr4 + '=$(openssl pkcs12 -in ' + f.path + ' -nomacver -nokeys -clcerts -nodes -password file:/tmp/' + randenvstr2 + ' | openssl x509 2>/dev/null); ' +
randenvstr5 + '=$(openssl pkcs12 -in ' + f.path + ' -nomacver -nokeys -cacerts -nodes -password file:/tmp/' + randenvstr2 + ' | openssl x509 2>/dev/null); ' +
'printf \'{"key": "%s","cert": "%s","chain": "%s", "err": "%s"}\' "$' + randenvstr3 + '" "$' + randenvstr4 + '" "$' + randenvstr5 + '" "$(cat /tmp/' + randenvstr6 + ')" ; rm /tmp/' + randenvstr6 + '; ' +
'rm /tmp/' + randenvstr2
 , {silent:true}, function(code, stdout, stderr) {

 // clean up the temp upload file 
 fs.remove (f.path, function (err) {
  if (err) {
	console.log ('error deleting ' + f.path + ': ' + err);
  }
 });

 var resdata = { resvalue: Buffer.from(stdout).toString('base64') };
 res.send(resdata);
 });

shelljs.env[randenvstr1] = '';
shelljs.env[randenvstr2] = '';
shelljs.env[randenvstr3] = '';
shelljs.env[randenvstr4] = '';
shelljs.env[randenvstr5] = '';
shelljs.env[randenvstr6] = '';

randenvstr1 = '';
randenvstr2 = '';
randenvstr3 = '';
randenvstr4 = '';
randenvstr5 = '';
randenvstr6 = '';

}); // post




app.route ('/api/v1/converttopfx') // generate the file, and send back the path and filename
.post(function(req,res,next){

var randenvstr1 = crypto.randomBytes(32).toString('hex');
var randenvstr2 = randenv();
var randenvstr3 = randenv();
var randenvstr4 = randenv();
var randenvstr5 = randenv();
var randenvstr6 = randenv();

shelljs.env[randenvstr2] = req.body.cp_key;
shelljs.env[randenvstr3] = req.body.cp_cert;
shelljs.env[randenvstr4] = req.body.cp_chain;
shelljs.env[randenvstr5] = req.body.cp_pwd;

var tempdir = __dirname + '/../download/' + randenvstr1;

fs.mkdirSync (tempdir);

shelljs.exec(
'echo "$' + randenvstr2 + '" > ' + tempdir + '/tempkey; ' +
'echo "$' + randenvstr3 + '" > ' + tempdir + '/tempcert; ' +
'echo "$' + randenvstr4 + '" > ' + tempdir + '/tempchain; ' +
'openssl pkcs12 -export -in ' + tempdir + '/tempcert -inkey ' + tempdir + '/tempkey -certfile ' + tempdir + '/tempchain -nodes -password pass:$' + randenvstr5 + ' -out ' + tempdir + '/$(openssl x509 -in ' + tempdir + '/tempcert -noout -subject | sed -n "/^subject/s/^.*CN=//p" | sed "s/\*/star/g").pfx ; echo "$?">/tmp/' + randenvstr6 + '; ' +
'printf \'{"randstr": "%s","filename": "%s","err": "%s"}\' "' + randenvstr1 + '" "$(openssl x509 -in ' + tempdir + '/tempcert -noout -subject | sed -n \'/^subject/s/^.*CN=//p\' | sed \'s/\*/star/g\').pfx" "$(cat /tmp/' + randenvstr6 + ')"; rm /tmp/' + randenvstr6
//'openssl x509 -in ' + tempdir + '/tempcert -noout -subject | sed -n "/^subject/s/^.*CN=//p" | sed "s/\*/star/g"'
, {silent:true},  function(code, stdout, stderr) {
res.send (stdout);

shelljs.env[randenvstr1] = '';
shelljs.env[randenvstr2] = '';
shelljs.env[randenvstr3] = '';
shelljs.env[randenvstr4] = '';
shelljs.env[randenvstr5] = '';

randenvstr1 = '';
randenvstr2 = '';
randenvstr3 = '';
randenvstr4 = '';
randenvstr5 = '';

});


});




app.route ('/api/v1/downloadpfx/:randstr/:filename') // generate the file, and send back the path and filename
.get(function(req,res,next){

var tempdir = __dirname + '/../download/' + req.params.randstr;

fs.pathExists (tempdir + '/' + req.params.filename, function(err, exists) {

if (exists) {

var file = fs.createReadStream(tempdir + '/' + req.params.filename);

file.on('end', function() {
	fs.remove (tempdir, function(err) {
	if (err) {
		console.log ('error deleting ' + req.params.filename + ': ' + err);	
		}
	});
});

file.pipe(res);

} else { // if exists

res.status(404).sendFile (path + '404.html');

} // else

}); // pathExists

}); // get


app.route ('/api/v1/getremotecert') // connect to remote endpoint and retrieve the PEM cert key
.post (function(req,res){

var check_url = geturlhost (req.body.check_url); // grab just the hostname - everything else is ignored (proto, port, uri, etc)

var randenvstr = randenv();
shelljs.env[randenvstr] = check_url;

// waterfall functions

function getremotecert_openssl (callback) {

shelljs.exec('openssl s_client -showcerts -connect "$' + randenvstr + '":443 -servername "$' + randenvstr + '" </dev/null'
, {silent:true},  function(code, stdout, stderr) {
	if (code != 0) {
		var errres = {
			rescode: code,
			resreason: 'Error encountered; Result code: ' + code
		}

		callback (errres); // send the openssl error code through to the waterfall end function

	} else { // no errors encountered in openssl, start parsing
		var lines = stdout.split('\n');
		lines = lines.filter(function(n){ return n !== undefined && n !== '' });

		var certs = stdout.match(/-----BEGIN CERTIFICATE-----(\n\S*)*-----END CERTIFICATE-----/g);

		callback (null, certs); // pass the matched PEM certs to the next function

	}
	}); // shelljs.exec

} // getremotecert_openssl


function getremotecert_parseloop (arg1, callback) {

// arg1 is an array of PEM certs

function readcert (item, callback1) {

	var parsedcert = pem.readCertificateInfo (item, function(readerr, output){

	if (readerr) {
		callback1 (readerr);
	} else { 

	// no errors, carry on

	var now = moment();

	var resultbundle = {};

	resultbundle.commonname = output.commonName;
	resultbundle.issuer = output.issuer.commonName;
	resultbundle.serial = output.serial.replace(/:/g,'');
	resultbundle.validfrom = moment(output.validity.start).format('MMMM Do YYYY');
	resultbundle.validto = moment(output.validity.end).format('MMMM Do YYYY');
	resultbundle.organization = output.organization;
	resultbundle.locality = output.locality;
	resultbundle.state = output.state;
	resultbundle.country = output.country;

	if (output.san !== undefined) {
		resultbundle.sans = output.san.dns;
	} else {
		resultbundle.sans = []; 
	}

	resultbundle.certbody = item;

	var certstatus = 0;
	var certduration = '';

	if (now < moment(output.validity.start)) {
		certstatus = 1; // Not active yet
		certstatusname = 'Not active yet';
	}

	if (now > moment(output.validity.end)) {
		certstatus = 2; // Expired
		certstatusname = 'Expired';
	}

	if (now > moment(output.validity.start) && now < moment(output.validity.end)) {
		certstatus = 3; // Valid
		certduration = moment.duration(now.diff(output.validity.end)).humanize() + ''; // humanized translation of how long until the cert expires
		certstatusname = 'Valid, expiring in ' + certduration;
	}

	resultbundle.certstatus = certstatus;
	resultbundle.certstatusname = certstatusname;
	resultbundle.certduration = certduration;

	if (resultbundle.commonname == resultbundle.issuer) {
		resultbundle.selfsigned = 1
	} else {
		resultbundle.selfsigned = 0;
	}

	callback1 (null, resultbundle);

	}

});


}; // async.map iteratee - readcert


async.map (arg1, readcert, function(err, results) {

// arg1 will be an array of objects containing info about each cert in the chain

	if (err) {
		callback (err);
	} else {	
		callback (null, results); // pass the array on as-is
	}

}); // async.map

} // getremotecert_parseloop


function getremotecert_dig (arg1, callback1) {

// arg1 is an array of objects

	dns.resolve4(check_url, function (err, records) {
	if (err) {
			callback1 (err);
		} else {
			callback1 (null, { arg1: arg1, records: records } );
		}
	});

}

async.waterfall ([
getremotecert_openssl,
getremotecert_parseloop,
getremotecert_dig
],
function (err, results) {
	if (err) {
		res.status(500).json({ rescode: 1, resreason: err });
	} else {
		res.status(200).send({ rescode: 0, resreason: 'Successful connection', check_host: check_url, results: results.arg1, ips: results.records });
	}
});



});




} // module.exports
