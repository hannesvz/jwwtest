
function geturlhost(url) {

var tu = url;

var protocheck = url.indexOf('://');

if (protocheck != -1) {
        tu = url.substring(protocheck + 3, url.length);
}

var ua = tu.split('');

var endofhostname = tu.length;

for (s = 0; s < ua.length; s++) {
        if (!/\w|\d|-|\./.test(ua[s]) || s == ua.length) {
                endofhostname = s;
                break;
        }
}

return (tu.substring(0,endofhostname));

}

function escapeSpecialChars(jsonString) {

    return jsonString.replace(/\n/g, "\\n")
        .replace(/\r/g, "\\r")
        .replace(/\t/g, "\\t")
        .replace(/\f/g, "\\f");
}

//ssl controllers

var sslApp = angular.module('sslApp', ['ngSanitize','ngFileUpload','ngclipboard', 'ngFileSaver']);

sslApp.controller('cckCtrl', ['$scope', '$http', function($scope, $http) {

$scope.cck_check = function() {
$scope.cck_resultbox ='Loading...';

var formdata = {cck_servername : $scope.cck_servername};

$http.post('/api/v1/checkcert', formdata).success(function(response) {
    $scope.cck_resultbox = response;
  });

};

}]);


sslApp.controller('ciCtrl', ['$scope', '$http', function( $scope, $http) {


$scope.ci_check = function(certcontent) {

var certtype = 0;

switch (true) { // figure out what type of certificate has been pasted into the form

 case (certcontent.substring(0,27) == '-----BEGIN CERTIFICATE-----' ):
  certtype = 1; // PEM
  break;

 case (certcontent.substring(0,35) == '-----BEGIN PKCS #7 SIGNED DATA-----'):
  certtype = 2; // P7B
 break;

 case (certcontent.substring(0,21) == '-----BEGIN PKCS7-----'):
  certtype = 2; // P7B
 break;

 case (certcontent.substring(0,35) == '-----BEGIN CERTIFICATE REQUEST-----' ):
  certtype = 3; // CSR
  break;

 default:
  certtype = 0; // whatever else or blank
  break;
  
}

switch (certtype) {

case 1:
case 2:

// slightly hacky, but the backend api call is similar enough to just toggle the url endpoint.

if (certtype == 1) { var endpoint = '/api/v1/certinfo'; }
if (certtype == 2) { var endpoint = '/api/v1/p7binfo'; }

var formdata = {ci_certinput : certcontent};

$http.post(endpoint, formdata).success(function(response) {

$scope.ci_type = 'Certificate ';

var resultstring;

if (response.error == '') {

resultstring = '';
resultstring += '<b>Common Name:</b><br>';
resultstring += response.commonname +'<br><br>';
resultstring += '<b>Organization:</b> ' + response.organization + '<br>';
resultstring += '<b>Locality:</b> ' + response.locality + '<br>';
resultstring += '<b>State:</b> ' + response.state + '<br>';
resultstring += '<b>Country:</b> ' + response.country + '<br><br>';
resultstring += '<b>Valid From:</b><br>';
resultstring += response.validfrom +'<br><br>';
resultstring += '<b>Valid To:</b><br>';
resultstring += response.validto +'<br><br>';
resultstring += '<b>Status:</b><br>';
resultstring += response.certstatus +'<br><br>';
resultstring += '<b>Issuer:</b><br>';
resultstring += response.issuer +'<br><br>';
resultstring += '<b>Serial Number:</b><br>';
resultstring += response.serial +'<br><br>';
if (response.sans.length > 1) {
resultstring += '<b>Subject Alternative Names:</b><br>';
resultstring += '<ul><li>' + response.sans.join ('<li>') +'</li></ul>';
}

} else {
 resultstring = 'Error: could not read certificate data';
}

$scope.ci_result = resultstring;

});

break; // case 1:

case 3:

var formdata = {ci_certinput : certcontent};
$http.post('/api/v1/csrinfo', formdata).success(function(response) {

$scope.ci_type = 'CSR ';

$scope.ci_result = '';
$scope.ci_result += '<b>Common Name:</b><br>';
$scope.ci_result += response.commonname +'<br><br>';
$scope.ci_result += '<b>Organization:</b> ' + response.organization + '<br>';
$scope.ci_result += '<b>Locality:</b> ' + response.locality + '<br>';
$scope.ci_result += '<b>State:</b> ' + response.state + '<br>';
$scope.ci_result += '<b>Country:</b> ' + response.country + '<br>';
});

break; // case 3:


case 0:
$scope.ci_result = '';
$scope.ci_type = '';


} // switch


};



$scope.ci_pfxmodal = function(certcontent,pfxfilename) {
// pfx has been dragged, pop up the modal

$('#pfxpwdmodal').modal('show');
$('#ci_modalprogress').hide();
$('#pfxpassword').removeAttr('readonly');

$scope.pfxpassword = '';

$('#pfxdecodeerror').hide();
$('#pfxname').text (' ' + pfxfilename);
$scope.pfxcertcontent = certcontent;
$scope.$apply();

}

/* ideally add yet another function to dismiss the modal, and then clear all the variables

xxx

*/

$scope.ci_pfxcheck = function(pfxpwd) { // the function that will be run from the modal, containing the password

var formdata = {
	ci_pfxinput : $scope.pfxcertcontent,
	ci_pfxpwd : pfxpwd
	};

$('#ci_modalprogress').show();
$('#pfxdecodeerror').hide();

$http.post('/api/v1/pfxinfo', formdata).success(function(response) {
	var resdecoded = JSON.parse(escapeSpecialChars(window.atob(response.resvalue))); // decode base64, escape the newlines, and parse to json object. phew
	if (resdecoded.err == "") {
		$scope.ci_certinput = resdecoded.cert;
		$scope.ci_check($scope.ci_certinput);
		$('#pfxpwdmodal').modal('hide');
		$('#ci_modalprogress').hide();
	} else {
		$('#pfxdecodeerror').show();
		$('#ci_modalprogress').hide();
	};	
	});

}




}]).directive('ciFileDropZone', [function () {
    return {
        restrict: 'EA',
        scope: {content:'='},
        link: function (scope, element, attrs) {

            //scope.content = "drop a .txt file here";

            var processDragOverOrEnter;

            processDragOverOrEnter = function (event) {
                if (event !== null) {
                    event.preventDefault();
                }
                event.dataTransfer.effectAllowed = 'copy';
                return false;
            };

            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);
            element.bind('drop', handleDropEvent);

            function insertText(loadedFile) {
		scope.content = loadedFile.target.result;
		if (scope.content.substring(0,5) == '-----') { // file types are being handled naively, and all are accepted and parsed, assuming they are certs. this check just makes sure.
                scope.$apply();
		scope.$parent.ci_check(scope.content); // run the ci_check function to refresh the data after the c_certinput field's value changed
		}
            }

            function handleDropEvent(event) {
                if (event !== null) {
                    event.preventDefault();
                }
                var reader = new FileReader();
		var pfxfilename = event.dataTransfer.files[0].name;
		if (event.dataTransfer.files[0].type == "application/x-pkcs12") { // a PFX has been dragged, upload it using Upload.upload
			reader.onload = function(fileref) { // define the function that will run when the file has been read completely
				scope.$parent.ci_pfxmodal(fileref.target.result,pfxfilename); // run the pfx_check function defined in the controller with the base64-encoded contents of the pfx
			}
			reader.readAsDataURL(event.dataTransfer.files[0]); //read the actual file

		} else { // something other than PFX was dragged, parse it as ascii
	                reader.onload = insertText;
        	        reader.readAsText(event.dataTransfer.files[0]);
		}
            }
        }
    };
}]);









sslApp.controller('ccrCtrl', ['$scope', '$http', function($scope, $http) {

$scope.ccr_check = function(certcontent) {

if (certcontent === '' || typeof certcontent === 'undefined') {
	$scope.ccr_resultbox1 = '';
	$scope.ccr_resultbox2 = '';
} else {

if (certcontent.substring(0,27) == '-----BEGIN CERTIFICATE-----' ) {

var formdata = {ccr_certinput : certcontent};
$http.post('/api/v1/certca', formdata).success(function(response) {
    $scope.ccr_resultbox1 = response.result1;
    $scope.ccr_resultbox2 = response.result2;
  });

} // check for cert

} // check for empty string

};


}]).directive('ccrFileDropZone', [function () {
    return {
        restrict: 'EA',
        scope: {content:'='},
        link: function (scope, element, attrs) {

            //scope.content = "drop a .txt file here";

            var processDragOverOrEnter;

            processDragOverOrEnter = function (event) {
                if (event !== null) {
                    event.preventDefault();
                }
                event.dataTransfer.effectAllowed = 'copy';
                return false;
            };

            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);
            element.bind('drop', handleDropEvent);

            function insertText(loadedFile) {
                scope.content = loadedFile.target.result;
                if (scope.content.substring(0,27) == '-----BEGIN CERTIFICATE-----') {
                scope.$apply();
                scope.$parent.ccr_check(scope.content); // run the ccr_check function to refresh the data after the ccr_certinput field's value changed
                }
            }

            function handleDropEvent(event) {

                if (event !== null) {
                    event.preventDefault();
                }
                var reader = new FileReader();
                reader.onload = insertText;
                reader.readAsText(event.dataTransfer.files[0]);
            }
        }
    };
}]);






sslApp.controller('ckmCtrl', ['$scope', '$http', function($scope, $http) {

function checkkeymatch() {

var icert = $scope.ckm_certmodhash;
var ikey = $scope.ckm_keymodhash;
 if (typeof icert === 'undefined' || typeof ikey === 'undefined') {
  $scope.ckm_resultbox = '';
 } else {

 if (icert == ikey) {
  $scope.ckm_resultbox = '<span class="glyphicon glyphicon-ok green"></span> <b>Hashes match!</b>';
 } else {
    $scope.ckm_resultbox = '<span class="glyphicon glyphicon-remove red"></span> Hashes do NOT match!';
   }
 }
}


$scope.ckm_checkcert = function(certcontent) {
if (certcontent.length > 0) {
if (certcontent.substring(0,27) == '-----BEGIN CERTIFICATE-----' || certcontent.substring(0,35) == '-----BEGIN CERTIFICATE REQUEST-----' ) {
$scope.ckm_certmodhash = '';
var formdata = {ckm_data: certcontent, ckm_type: 'x509'}; //ckm_type not really used anymore, but is x509 for all things entered into the left-hand box (ckm_certinput)
$http.post('/api/v1/modhash', formdata).success(function(response) {
  $scope.ckm_certmodhash = response.modhash;
  $scope.ckm_resultboxcn = '<b>CN:</b> ' + response.commonname;
  checkkeymatch();
  });
} else {
$scope.ckm_certmodhash = '';
$scope.ckm_resultboxcn = '';
$scope.ckm_resultbox = '';
}
} else {
$scope.ckm_certmodhash = '';
$scope.ckm_resultboxcn = '';
$scope.ckm_resultbox = '';
} // length > 0
};



$scope.ckm_checkkey = function(keycontent) {

var keytype = 0;

//$scope.btnconvertkeydisabled = true;

if (keycontent == '') keytype = 1; // empty

if (keycontent.substring(0,27) == '-----BEGIN PRIVATE KEY-----' ) keytype = 2;

if (keycontent.substring(0,31) == '-----BEGIN RSA PRIVATE KEY-----' ) keytype = 3;

switch (keytype) {

  case 1:
    $scope.ckm_keymodhash = '';
    $scope.ckm_resultbox = '';
    $scope.btnconvertkeydisabled = true;
    break;

  case 2:
    $scope.ckm_keymodhash = '';
    $scope.ckm_certmodhash = '';
    $scope.btnconvertkeydisabled = false;
    break;

  case 3:
    $scope.ckm_keymodhash = '';
    var formdata = {ckm_data: keycontent, ckm_type: 'rsa'};
    $http.post('/api/v1/modhash', formdata).success(function(response) {
      if (response.rescode == 0) {
	$scope.ckm_keymodhash = response.modhash;
	if ($scope.ckm_certinput != '') {
		checkkeymatch();
	} else { // code is 0, but cert is empty, so clear the resultbox
		$scope.ckm_resultbox = '';
	}

	} else { //rescode is not 0
		$scope.ckm_resultbox = '<span class="glyphicon glyphicon-remove red"></span> This key appears to be corrupt';
	}

	      });

}

};


// convert to RSA button clicked

$scope.ckm_convertkey = function() {

var convformdata = { ckm_keyconvertinput: $scope.ckm_keyinput }

$http.post('/api/v1/convertkey', convformdata).success(function(convresponse) {

if (convresponse.rescode == 0) {
$scope.ckm_keymodhash = '';
  $scope.ckm_keyinput = convresponse.keyoutput;

    var formdata = {ckm_data: $scope.ckm_keyinput, ckm_type: 'rsa'};

    $http.post('/api/v1/modhash', formdata).success(function(response) {
      $scope.ckm_keymodhash = response.modhash;

      if ($scope.ckm_certinput != "" && typeof $scope.ckm_certinput != 'undefined') {
	checkkeymatch();
        }

      });
} else {
 $scope.ckm_resultbox = '<span class="glyphicon glyphicon-remove red"></span> This key appears to be corrupt';
}


  });

}


}]).directive('ckmcFileDropZone', [function () {
    return {
        restrict: 'EA',
        scope: {content:'='},
        link: function (scope, element, attrs) {
            
            //scope.content = "drop a .txt file here";
            
            var processDragOverOrEnter;
            
            processDragOverOrEnter = function (event) {
                if (event !== null) {
                    event.preventDefault();
                }
                event.dataTransfer.effectAllowed = 'copy';
                return false;
            };
            
            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);
            element.bind('drop', handleDropEvent);
            
            function insertText(loadedFile) {
                scope.content = loadedFile.target.result;
                if (scope.content.substring(0,27) == '-----BEGIN CERTIFICATE-----' || scope.content.substring(0,35) == '-----BEGIN CERTIFICATE REQUEST-----' ) {
                scope.$apply();
                scope.$parent.ckm_checkcert(scope.content); // run the ckm_checkcert function to refresh the data after the ckm_certinput field's value changed
                }
            }
            
            function handleDropEvent(event) {
                
                if (event !== null) {
                    event.preventDefault();
                }
                var reader = new FileReader();
                reader.onload = insertText;
                reader.readAsText(event.dataTransfer.files[0]);
            }
        }
    };
}]).directive('ckmkFileDropZone', [function () {
    return {
        restrict: 'EA',
        scope: {content:'='},
        link: function (scope, element, attrs) {

            //scope.content = "drop a .txt file here";

            var processDragOverOrEnter;

            processDragOverOrEnter = function (event) {
                if (event !== null) {
                    event.preventDefault();
                }
                event.dataTransfer.effectAllowed = 'copy';
                return false;
            };

            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);
            element.bind('drop', handleDropEvent);

            function insertText(loadedFile) {
                scope.content = loadedFile.target.result;
                if ( scope.content.substring(0,27) == '-----BEGIN PRIVATE KEY-----' || scope.content.substring(0,31) == '-----BEGIN RSA PRIVATE KEY-----' ) {
                scope.$apply();
                scope.$parent.ckm_checkkey(scope.content); // run the ckm_checkkey function to refresh the data after the ckm_keyinput field's value changed
                }
            }

            function handleDropEvent(event) {

                if (event !== null) {
                    event.preventDefault();
                }
                var reader = new FileReader();
                reader.onload = insertText;
                reader.readAsText(event.dataTransfer.files[0]);
            }
        }
    };
}]);








sslApp.controller('ccvCtrl', ['$scope', '$http', function($scope, $http) {

$scope.ccv_convert = function() {
var formdata = {ccv_input : $scope.ccv_input} ;


$http.post ('/certconvert', formdata).success (function(response) {
$scope.ccv_output = response.output;
});

}

}]);





sslApp.controller('cpCtrl', ['$scope', '$http', 'Upload', 'FileSaver', 'Blob', function($scope, $http, Upload, FileSaver) {

$scope.cp_pfxmodal = function($files) {
// pfx has been dragged, pop up the modal

$scope.$files = $files;

if ($files[0].type == 'application/x-pkcs12') {
$('#cp_pfxpwdmodal').modal('show');
$('#cp_pfxpassword').text (' ');

$scope.pfxpassword = '';

$('#cp_pfxmodalprogress').hide();
$('#cp_pfxdecodeerror').hide();
$('#cp_pfxname').text (' ' + $files[0].name);
$scope.$apply();

}


if ($files[0].type == 'application/x-pkcs7-certificates') {

$scope.cp_p7bsplit ($files);

}

}


$scope.cp_p7bsplit = function($files) {

var read = new FileReader();

read.readAsBinaryString($files[0]);

read.onloadend = function() {

var formdata = {cp_certdata: read.result };
$http.post('/api/v1/p7bsplit', formdata).success(function(response) {

switch (response.rescode) {
 case 0:
 $scope.cp_cert = response.cert;
 $scope.cp_chain = response.chain;
 $scope.cp_check (response.cert);
 break;

 default:
 $scope.cp_result = 'Error: could not read certificate data';

}

});

}

}


$scope.cp_pfxupload = function(pfxpassword) {

$('#cp_pfxmodalprogress').show();
$('#cp_pfxdecodeerror').hide(); // freshly submitted, clear the error if it was there from a previous failure

if ($scope.$files.length > 0) { // check if a file was selected

  $scope.cp_resultbox = ''; // blank out result box

  Upload.upload({
    url: '/api/v1/cppfxupload',
    data: { 'pwd': pfxpassword },
    file: $scope.$files
  }).then(function(data, status, headers, config) {
    // file is uploaded successfully
	$scope.cp_fileselector = ''; // clear dropzone
	var resdecoded = JSON.parse(escapeSpecialChars(window.atob(data.data.resvalue))); // decode base64, escape the newlines, and parse to json object. phew
        if (resdecoded.err == "") {
		$scope.cp_check (resdecoded.cert);
		$scope.cp_cert = resdecoded.cert;
		$scope.cp_key = resdecoded.key;
		$scope.cp_chain = resdecoded.chain;
		$('#cp_pfxmodalprogress').hide();
                $('#cp_pfxpwdmodal').modal('hide');
        } else {
		$('#cp_pfxmodalprogress').hide();
                $('#cp_pfxdecodeerror').show();
        };
	//$scope.fu_resultbox = data.data.resdata; // populate cert, key, chain textareas
  });


} else {
// files object was empty, do nothing
}

}


$scope.cp_check = function(certcontent) {
if (certcontent.substring(0,27) == '-----BEGIN CERTIFICATE-----' ) {
var formdata = {ci_certinput : certcontent};
$http.post('/api/v1/certinfo', formdata).success(function(response) {

var resultstring;

if (response.error == '') {

resultstring = '';
resultstring += '<b>Common Name:</b><br>';
resultstring += response.commonname +'<br><br>';
resultstring += '<b>Organization:</b> ' + response.organization + '<br>';
resultstring += '<b>Locality:</b> ' + response.locality + '<br>';
resultstring += '<b>State:</b> ' + response.state + '<br>';
resultstring += '<b>Country:</b> ' + response.country + '<br><br>';
resultstring += '<b>Valid From:</b><br>';
resultstring += response.validfrom +'<br><br>';
resultstring += '<b>Valid To:</b><br>';
resultstring += response.validto +'<br><br>';
resultstring += '<b>Status:</b><br>';
resultstring += response.certstatus +'<br><br>';
resultstring += '<b>Issuer:</b><br>';
resultstring += response.issuer +'<br><br>';
resultstring += '<b>Serial Number:</b><br>';
resultstring += response.serial +'<br><br>';
if (response.sans.length > 1) {
resultstring += '<b>Subject Alternative Names:</b><br>';
resultstring += '<ul><li>' + response.sans.join ('<li>') +'</li></ul>';
}

} else {
 resultstring = 'Error: could not read certificate data';
}

$scope.cp_result = resultstring;

});
$scope.btngetchaindisabled = false;
} else {

$scope.btngetchaindisabled = true;

}

if (certcontent == '') {
$scope.cp_result = '';
}

};


// download function for textareas
// thanks bro http://alferov.github.io/angular-file-saver/
$scope.cp_download = function(text,name) {
        var data = new Blob([text], { type: 'text/plain;charset=utf-8' });
        FileSaver.saveAs(data, name);
        };



// function to pop up the modal when converting TO pfx
$scope.cp_convertpfxmodal = function() {

$('#cp_pfxconvertpassword').removeAttr('readonly');
$('#cp_pfxconvertpwdmodal').modal('show');
$('#cp_pfxconvertmodalprogress').hide();
$('#cp_pfxconverterror').hide();
$scope.pfxconvertpassword = '';

} // cp_convertpfxmodal



// will be executed from the modal popup submit button / enter press
$scope.cp_converttopfx = function(cp_pwd) { 

var formdata = {cp_key : $scope.cp_key,
		cp_cert : $scope.cp_cert,
		cp_chain : $scope.cp_chain,
		cp_pwd : cp_pwd };

$('#cp_pfxconverterror').hide(); // hide the error on a fresh submit, if it was there from a previous error
$('#cp_pfxconvertmodalprogress').show();

// post the 3 components and create the pfx, expecting back the random check string (the folder name) and the file name
$http.post('/api/v1/converttopfx', formdata).success(function(response) { 

if (response.err != 0) { // something went wrong creating the pfx, openssl output was not 0

$('#cp_pfxconverterror').show();
$('#cp_pfxconvertmodalprogress').hide();

} else {

// submitting, blank the password and hide the modal
$scope.pfxconvertpassword = '';
$('#cp_pfxconvertmodalprogress').hide();
$('#cp_pfxconvertpwdmodal').modal('hide');

var filePath = '/api/v1/downloadpfx/' + response.randstr + '/' + response.filename;

//fake an anchor tag and click it, starting the file download to /downloadpfx with the random string and file name - thanks stackexchange

var link = document.createElement('a');
  link.href = filePath;
  link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
  link.click();

} // else (err was 0)

});

} //cp_converttopfx


$scope.cp_getchain = function(cert) {

$scope.cp_getchainicon = 'fa fa-spinner fa-spin';

var formdata = {ccr_certinput : cert};

$http.post('/api/v1/certca', formdata).success(function(response) {
    $scope.cp_chain = response.result2;
    $scope.cp_getchainicon = 'glyphicon glyphicon-link';
  });


}



}]).directive('cpkeyFileDropZone', [function () { // tack on the directives for drag and drop. keep two for different validation rules, if necessary in the future.
    return {
        restrict: 'EA',
        scope: {content:'='},
        link: function (scope, element, attrs) {
            var processDragOverOrEnter;

            processDragOverOrEnter = function (event) {
                if (event !== null) {
                    event.preventDefault();
                }
                event.dataTransfer.effectAllowed = 'copy';
                return false;
            };

            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);
            element.bind('drop', handleDropEvent);

            function insertText(loadedFile) {
                scope.content = loadedFile.target.result;
		scope.$apply(); // turns out this is very important
/*
               if (scope.content.substring(0,27) == '-----BEGIN CERTIFICATE-----') {
                scope.$apply();
                }
*/ //extra validation after dropping data
            }

            function handleDropEvent(event) {

                if (event !== null) {
                    event.preventDefault();
                }
                var reader = new FileReader();
                reader.onload = insertText;
                reader.readAsText(event.dataTransfer.files[0]);
            }
        }
    };
}]).directive('cpcertFileDropZone', [function () {
    return {
        restrict: 'EA',
        scope: {content:'='},
        link: function (scope, element, attrs) {
            var processDragOverOrEnter;

            processDragOverOrEnter = function (event) {
                if (event !== null) {
                    event.preventDefault();
                }
                event.dataTransfer.effectAllowed = 'copy';
                return false;
            };

            element.bind('dragover', processDragOverOrEnter);
            element.bind('dragenter', processDragOverOrEnter);
            element.bind('drop', handleDropEvent);

            function insertText(loadedFile) {
                scope.content = loadedFile.target.result;
		scope.$apply(); // turns out this is very important
/*              
               if (scope.content.substring(0,27) == '-----BEGIN CERTIFICATE-----') {
                scope.$apply();
                }
*/ //extra validation after dropping data
            }

            function handleDropEvent(event) {

                if (event !== null) {
                    event.preventDefault();
                }
                var reader = new FileReader();
                reader.onload = insertText;
                reader.readAsText(event.dataTransfer.files[0]);
            }
        }
    };
}]);


