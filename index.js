const config = require("platformsh-config").config();

const express = require("express");
const app = express();

const fs = require('fs-extra');

function read_base64_json(varName) {
  try {
    return JSON.parse(Buffer(process.env[varName], 'base64').toString());
  } catch (err) {
    throw new Error(`no ${varName} environment variable`);
  }
};

const variables = read_base64_json('PLATFORM_VARIABLES');

console.log ('variables read: ', variables);

/*
var morgan = require('morgan');
var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'});
app.use(morgan('combined', {stream: accessLogStream}));
*/

const robots = require('express-robots');

app.use(robots({UserAgent: '*', CrawlDelay: '5'}))

const favicon = require('serve-favicon');
app.use(favicon(__dirname + '/public/favicon.ico'));

app.use('/js', express.static(__dirname + '/js/'));
app.use('/public', express.static(__dirname + '/public/'));

const helmet = require('helmet');
app.use (helmet());

const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

const moment = require('moment');

const async = require('async');

const filepath = require('path');
const mime = require('mime');

const shelljs = require('shelljs');

const cp = require("child_process");

const exec = require('child_process').exec;

const path = __dirname + '/public/';

const request = require("request");

function randenv() {
        return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
}

app.route ("/")
.get((req,res) => {
res.sendFile(path + 'ssl.html');
});

app.route ("/ssl")
.get((req,res) => {
res.sendFile(path + 'ssl.html');
});

require(__dirname + '/routes/ssl.js')(app);

app.use("*", (req,res) => {
  res.sendFile(path + '404.html');
});

console.log ('node server running');
app.listen(config.port);



